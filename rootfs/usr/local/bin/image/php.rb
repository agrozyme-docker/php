# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Php
      module Build
        # @param [String] prefix
        # @param [String] suffix
        def self.php_tag(prefix = '', suffix = '')
          return "#{prefix}#{ENV['PHP_MAJOR_VERSION']}#{suffix}"
        end

        def self.download_composer
          home = ENV['COMPOSER_HOME']
          file = '/usr/local/bin/composer'
          link = 'https://composer.github.io'

          Shell.make_folders(home)
          Shell.download_file('https://getcomposer.org/composer-stable.phar', file)

          { snapshots: 'dev', releases: 'tags' }.each do |key, value|
            Shell.download_file("#{link}/#{key}.pub", "#{home}/keys.#{value}.pub")
          end

          Shell.change_owner(home)
          Shell.change_mode('+x', file)
        end

        def self.setup_composer
          System.run('composer diagnose')
          System.run('composer clear-cache')
        end

        def self.update_setting
          etc = self.php_tag('/etc/php')
          docker = "#{etc}/docker"
          fpm = 'php-fpm.conf'
          log = '/var/log/php'

          items = "#{ENV['PHP_INI_SCAN_DIR']}".split(':').filter do |path|
            next "#{etc}/conf.d" != path
          end.map do |path|
            next "include=#{path}/*.conf"
          end

          File.open("#{etc}/#{fpm}", 'a') do |file|
            file.puts(items)
          end

          File.open("#{docker}/00_#{fpm}", 'a') do |file|
            file.puts(self.php_tag("error_log = #{log}", '/error.log'))
          end

          File.open("#{docker}/01_www.conf", 'a') do |file|
            file.puts(self.php_tag("access.log = #{log}", '/access.log'))
          end
        end

        def self.php_packages
          items = System.capture('apk search', { 'no-cache': true, exact: true, quiet: true }, self.php_tag('php', '-*')).lines(chomp: true)

          items = items.filter do |item|
            # next false == item.match?(/(-apache2|-cgi|-dev|-doc|-litespeed|-static|-pecl-gmagick)$/)
            next false == item.match?(/(-apache2|-cgi|-dev|-doc|-litespeed|-static|-pecl-psr)$/)
          end

          # items = items.filter do |item|
          #   next false == item.match?(/-pecl-/)
          # end

          # items.unshift('icu-data-full', 'imagemagick')
          items.unshift('icu-data-full')
          return items.uniq.sort
        end

        def self.main
          bin = '/usr/bin/php'
          etc = '/etc/php'
          folder = self.php_tag('/var/log/php')

          System.run('apk add', { 'no-cache': true }, self.php_packages)

          if false == File.exist?(bin)
            File.symlink(self.php_tag(bin), bin)
          end

          # FileUtils.mv(self.php_tag('/usr/lib/php', '/modules/libcouchbase_php_core.so'), '/usr/lib')
          Shell.move_paths("#{etc}/*", self.php_tag(etc))
          Shell.remove_paths(etc)
          Shell.make_folders(folder, self.php_tag('/usr/local/etc/'), '/var/www/html', '/usr/local/lib/composer/cache')
          Shell.link_logs(stdout: "#{folder}/access.log", stderr: "#{folder}/error.log")
          System.invoke('Update Setting', self.method(:update_setting))
          System.invoke('Download Composer', self.method(:download_composer))
          System.invoke('Setup Composer', self.method(:setup_composer))
        end

      end

      module Run
        def self.main
          fpm = Build.php_tag('php-fpm')

          Shell.update_user
          Shell.clear_folders("/run/#{fpm}")
          System.execute(fpm, { nodaemonize: true }, '')
        end
      end
    end
  end
end
