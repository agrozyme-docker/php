# Summary

Source: https://gitlab.com/agrozyme-docker/php

While designed for web development, the PHP scripting language also provides general-purpose use.

# Settings

- Mode: PHP-CLI / PHP-FPM
- DocumentRoot Directory: `/var/www/html`
- Default Configuration Directory: `/etc/php81`
- Custom PHP-FPM Configuration Files: `/usr/local/etc/php81/*.conf`
- Custom .ini Configuration Files: `/usr/local/etc/php81/*.ini`

# Commands

- [php](https://www.php.net/manual/en/features.commandline.options.php)
- [composer](https://getcomposer.org/doc/03-cli.md)

# Environment Variables

- COMPOSER_HOME: `/usr/local/lib/composer`
