ARG DOCKER_REGISTRY=docker.io
ARG DOCKER_NAMESPACE=agrozyme
FROM ${DOCKER_REGISTRY}/${DOCKER_NAMESPACE}/alpine
COPY rootfs /
ENV \
  PHP_MAJOR_VERSION=84 \
  COMPOSER_ALLOW_SUPERUSER=1 \
  COMPOSER_MEMORY_LIMIT=-1 \
  COMPOSER_HOME=/usr/local/lib/composer

ENV \
  PHP_INI_SCAN_DIR="/etc/php${PHP_MAJOR_VERSION}/conf.d:/etc/php${PHP_MAJOR_VERSION}/docker:/usr/local/etc/php${PHP_MAJOR_VERSION}" \
  PATH="${PATH}:${COMPOSER_HOME}/vendor/bin"

RUN chmod +x /usr/local/bin/* \
  && gem update -N docker_core \
  && gem clean \
  && /usr/local/bin/docker_build.rb
WORKDIR /var/www/html
EXPOSE 9000
CMD ["/usr/local/bin/docker_run.rb"]
